

public class Game{
	private int[] score;
	private int shot;
	
	public Game(){
		score = new int[21];
		for(int i=0;i<21;i++){
			score[i] = 0;
		}
		shot = 0;
	}
	
	public void roll(int pins){
		if(pins >= 0 && shot < 20){
			boolean isStrike = (pins == 10 && shot % 2 == 0);
			if(isStrike){
				//strike
				if(shot !=20) score[shot+1] = 0;
			}			
			score[shot] = pins;
			shot++;
			if(isStrike){
				if(shot<18) shot++;
			}
		}
	}
	
	
	public int score(){
		int score = 0;
		for(int i = 0; i<18 ;i+=2){
			if(this.score[i] == 10){
				//strike
				score += frameScore(i)+frameScore(i+2)+frameScore(i+4);
			}else if(this.score[i] + this.score[i+1] == 10){
				//spare
				score += frameScore(i)+frameScore(i+2);
			}
			else{
				//punteggio normale
				score += this.score[i] + this.score[i+1];
			}
		}
		score+= frameScore(18);
		if(this.score[18]+this.score[19] == 10) score+= this.score[19];
		return score;
	}
	
	private int frameScore(int i){
		if(i<18){
			return score[i]+score[i+1];
		}else if (i==18){
			return score[18]+score[19]+score[20];
		}else{
			return score[20];
		}
	}
}